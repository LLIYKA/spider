﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ThreadSafeSleep;

namespace SMSpider
{
  class ConnModem
  {
    private SerialPort _serialPort;
    public int PortNum { get; set; }
    public bool IsConnected { get; }
    public int dellay { get; set; }
    public ConnModem(int PortNum, int dellay)
    {
      this.PortNum = PortNum;
      this.dellay = dellay;
      _serialPort = new SerialPort();

      _serialPort.BaudRate = 2400; // еще варианты 4800, 9600, 28800 или 56000
      _serialPort.DataBits = 7; // еще варианты 8, 9

      _serialPort.StopBits = StopBits.One; // еще варианты StopBits.Two StopBits.None или StopBits.OnePointFive         
      _serialPort.Parity = Parity.Odd; // еще варианты Parity.Even Parity.Mark Parity.None или Parity.Space

      _serialPort.ReadTimeout = dellay; // самый оптимальный промежуток времени
      _serialPort.WriteTimeout = dellay; // самый оптимальный промежуток времени

      _serialPort.Encoding = Encoding.GetEncoding("windows-1251");
      _serialPort.PortName = "COM" + this.PortNum;
      // _serialPort.DataReceived += SerialPortDataReceived;

      // незамысловатая конструкция для открытия порта
      if (_serialPort.IsOpen)
        _serialPort.Close(); // он мог быть открыт с другими параметрами
      try
      {
        _serialPort.Open();
      }
      catch (Exception e) { }
    }
    ~ConnModem()
    {
      Close();
    }
    public void Close()
    {
      if (_serialPort.IsOpen)
        _serialPort.Close();
    }
    public string SendAT(string command)
    {
      _serialPort.WriteLine(command+"\r\n");
      ThreadSafe.Sleep(dellay);
      var outText = _serialPort.ReadExisting();
      Console.WriteLine(outText);
      if (ThreadSafe.Stop)
        Close();
      return outText;
    }
  
  
  }
}