﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ThreadSafeSleep;

namespace SMSpider
{
  class Spider
  {
    private string _pathToFile
    {
      get { return ConfigurationManager.AppSettings.Get("pathToFile"); }
    }
    public string PathToFile
    {
      get
      {
        return DirectoryPath + "\\" + DateTime.Now.ToShortDateString()+".txt";
      }
    }
    public string DirectoryPath
    {
      get
      {
        return Path.GetDirectoryName(_pathToFile);
      }
    }
    public int countDay
    {
      get
      {
        var countDay = 5;
        try
        {
          countDay = int.Parse(ConfigurationManager.AppSettings.Get("CountDayLifeFile"));
        }
        catch { }
        return countDay;
      }
    }
    public void weave()
    {
      do
      {
        var _comNum = int.Parse(ConfigurationManager.AppSettings.Get("_comNum"));
        var _dellay = int.Parse(ConfigurationManager.AppSettings.Get("_dellay"));
        var modemOnce = new ConnModem(_comNum, _dellay);
        modemOnce.SendAT("AT");
        modemOnce.SendAT("AT+CMGF=1");//1 = вкл текстовый режим|| 0 = выкл
        modemOnce.SendAT("AT+CPMS=\"SM\",\"SM\",\"SM\"");//настраиваем 
        var _text_ListMessages = modemOnce.SendAT("AT+CMGL=\"ALL\"");//настраиваем    
        var ListMessageIndex = GetIndexMessage(_text_ListMessages);

        foreach (var itemMsg in ListMessageIndex)
        {
          var msgText = modemOnce.SendAT("AT+CMGR=" + itemMsg);//читаем
          if (StoreMessage(msgText))
            modemOnce.SendAT("AT+CMGD=" + itemMsg);//удаляем
        }
        modemOnce.Close();
        var SleppTime = int.Parse(ConfigurationManager.AppSettings.Get("Sleep"));
        ThreadSafe.Sleep(SleppTime);
        TrimAlterFIle();
      } while (!ThreadSafe.Stop);
    }

    private List<int> GetIndexMessage(string listMessages)
    {
      var ListMsgIndex = new List<int>();
      var regMsgInex = new Regex(@"(?<=\+CMGL:\s)\d*");
      var MatchesMsg = regMsgInex.Matches(listMessages);
      foreach (Match itemMatch in MatchesMsg)
      {
        var index = itemMatch.Groups[0].Value;
        if (index != "")
          ListMsgIndex.Add(int.Parse(index));
      }
      return ListMsgIndex;
    }
    private bool StoreMessage(string message)
    {
      message = message.Replace("OK", "");
      bool messageStored = true;
      try
      {
        var RegBodyMsg = new Regex(@"(?<=""\r\n)[0-9A-Fa-f]{4,}");
        var RegIsCancell = new Regex(@"(?<=\+CMGR:).*[\r\n]*.*операция[\s0-9\.]*?-[\s0-9\.]*?");
        var RegRest = new Regex(@"[\r\n][\w]*:?[0-9\s\.\,]{3,}$");
        var RegBankPhone = new Regex(@".*,""900""");

        var bodyMessage = DecodeSMS(RegBodyMsg.Match(message).Groups[0].Value);

        message = RegBodyMsg.Split(message)[0] + bodyMessage;
        if (!RegIsCancell.IsMatch(message) && !RegRest.IsMatch(message) && RegBankPhone.IsMatch(message))
        {
          File.AppendAllText(PathToFile, message + "\r\n");
        }
      }
      catch { messageStored = false; }
      return messageStored;
    }
    public string DecodeSMS(string smsText)
    {
      string unicodeString = "";
      for (int i = 0; i < smsText.Length; i += 4)
      {
        int code = int.Parse(smsText.Substring(i, 4), System.Globalization.NumberStyles.HexNumber);
        unicodeString += char.ConvertFromUtf32(code);
      }
      return unicodeString;
    }
    private void TrimAlterFIle()
    {
      var todayFiles = Directory.GetFiles(DirectoryPath)
                 .Where(x =>
                 {
                   var fileinf = new FileInfo(x);
                   return fileinf.Extension == ".txt" && fileinf.CreationTime.Date <= DateTime.Now.Date.AddDays(-countDay);
                 }
                 );
      foreach (var itemFile in todayFiles)
      {
        File.Delete(itemFile);
      }
    }
  }
}
