﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration.Install;
using System.ComponentModel;
using System.ServiceProcess;


[RunInstaller(true)]
public class SpiderInstaller : Installer
{
  private ServiceProcessInstaller processInstaller;
  private ServiceInstaller serviceInstaller;

  public SpiderInstaller()
  {
    processInstaller = new ServiceProcessInstaller();
    serviceInstaller = new ServiceInstaller();

    processInstaller.Account = ServiceAccount.LocalSystem;
    serviceInstaller.StartType = ServiceStartMode.Manual;
    serviceInstaller.ServiceName = "SMSpierd"; //must match CronService.ServiceName

    Installers.Add(serviceInstaller);
    Installers.Add(processInstaller);
  }
}