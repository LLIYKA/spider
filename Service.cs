﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceProcess;
using System.Threading;
using System.Net;
using ThreadSafeSleep;

namespace SMSpider
{
  class Service : ServiceBase
  {
    public Service()
    {
      this.ServiceName = "SMSpierd";
      this.CanStop = true;
      this.CanPauseAndContinue = false;
      this.AutoLog = true;
    }
    protected override void OnStart(string[] args)
    {
      // TODO: add startup stuff
      var _spider = new Spider();
      
      Thread t = new Thread(new ThreadStart(_spider.weave));

      t.Start();

    }

    protected override void OnStop()
    {
      ThreadSafe.Stop = true;
    }

  }
}